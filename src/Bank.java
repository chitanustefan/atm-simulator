package tema4.tema4;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Bank implements BankProc, Serializable{


	/**
	 * @invariant mapper.containsKey(p) == false implies mapper.get(p) == null
	 */
	
	private static final long serialVersionUID = 1L;
	
	
	public static HashMap <Person, ArrayList<Account>> mapper ; 
	public Bank() {	
		
		this.mapper = new HashMap<Person, ArrayList<Account>>();
		
	}
	
	public void addPerson(Person p) {
		ArrayList<Account> accList = new ArrayList<Account>();
		int size1 = mapper.size();
		mapper.put(p,accList);
		int size2 = mapper.size();
		assert size2 == size1 + 1 : "Adaugare persoana esuata";	
		System.out.println("Persoana adaugata");	
		
	}

	public void removePerson(Person p) {
		int size1 = mapper.size();
		mapper.remove(p);
		int size2 = mapper.size();
		assert size2 == size1 - 1 : "Stergere persoana esuata";	
	    System.out.println("Persoana stearsa");	
		
	}

	public void editPerson(Person oldP, Person newP) {
		ArrayList<Account> o = mapper.remove(oldP);
		mapper.put(newP, o);
	}
	
	public void addAcc(Account a, Person p) {
		
		int size1 = mapper.get(p).size();
		mapper.get(p).add(a);
		int size2 = mapper.get(p).size();
		assert size2 == size1 + 1 : "Esuare adaugare cont";
		System.out.println("Cont adaugat cu succes");
		
	}

	public void removeAcc(Person p, String iban) {
		int size1 = mapper.get(p).size();		
		for(int i = 0 ; i < mapper.get(p).size(); i++) {
			if(iban.equals(mapper.get(p).get(i).getIban()))
				mapper.get(p).remove(i);
		}
		int size2 = mapper.get(p).size();
		assert size2 == size1 - 1 : "Esuare stergere cont";
		System.out.println("Cont sters cu succes");
		
	}
	
	public void writeAcc(String iban, int sum, String c) {
		boolean dep = false;
		boolean wit = false;
    	for (HashMap.Entry<Person, ArrayList<Account>> entry : mapper.entrySet()) {
    		Iterator<Account> it = entry.getValue().iterator();
    		while(it.hasNext()) {
    			Account a = it.next();
    			int s1 = a.getSuma();
    			if(a.getIban().equals(iban)) {
    				if(a instanceof SpendingAccount) {
    					SpendingAccount a1 = (SpendingAccount) a;
    					if(c.equals("deposit")) {
    						a1.addRelatedCustomer(entry.getKey());
    						a1.deposit(sum);
    						int s2 = a1.getSuma();
    						assert s2 >= s1 : "Deposit esuat";
    					}		
    					else {
    						int s3 = a1.getSuma();
    						a1.addRelatedCustomer(entry.getKey());
    						a1.withDraw(sum);
    						assert s1 <= s3 : "Retragere esuata";
    					}
    				}	
    				if(a instanceof SavingAccount) {
    					SavingAccount a2 = (SavingAccount) a;
    					if(dep == false) {
    						if(c.equals("deposit")) {
    							a2.addRelatedCustomer(entry.getKey());
    							a2.deposit(sum);
    							int s2 = a2.getSuma();
        						assert s2 >= s1 : "Deposit esuat";
    							dep=true;
    						}
    					}	
    					if(wit == false) {
    						if(c.equals("withdraw")) {
    							int s3 = a2.getSuma();
    							a2.addRelatedCustomer(entry.getKey());
    							a2.withDraw(sum);
    							assert s1 <= s3 : "Retragere esuata";
    							wit=true;
    						}
    					}
    				}
    			}
    		}		
    	}	
	}	  
}
