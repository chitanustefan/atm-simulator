package tema4.tema4;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void test() {
		
		Bank b = new Bank();
		
		Person p1 = new Person("Stefan", "Petru","123");
		Person p2 = new Person("Vlad", "Andrei","456");
		Person p3 = new Person("Costea", "Bogdan","789");
		
		Account a1 = new SavingAccount("iban1", "Vlad", "456", 0, "Saving");
		Account a2 = new SpendingAccount("iban2", "Vlad", "456", 0, "Spending");
		Account a3 = new SavingAccount("iban3", "Costea", "789", 0, "Saving");
			
		b.addPerson(p1);
		b.addPerson(p2);
		b.addPerson(p3);
		b.removePerson(p1);
		
		b.addAcc(a1, p2);
		b.addAcc(a2, p2);
		b.addAcc(a3, p3);
		b.removeAcc(p2, "iban1");
		
		b.writeAcc("iban2", 100, "deposit");
	//	b.writeAcc("iban3", 400, "deposit");
		b.writeAcc("iban3", 200, "withdraw");
		
		//assertTrue(Bank.mapper.containsKey(p1) && Bank.mapper.get(p1) != null);
		
		
	}

}
