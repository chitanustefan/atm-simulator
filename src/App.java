package tema4.tema4;

import java.awt.BorderLayout;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Bank b = new Bank();
    	View view = new View(b);	
    	try
        {
           FileInputStream fis = new FileInputStream("hashmap.ser");
           ObjectInputStream ois = new ObjectInputStream(fis);
           HashMap<Person, ArrayList<Account>> readObject = (HashMap<Person, ArrayList<Account>>) ois.readObject();
		   Bank.mapper = readObject;
           ois.close();
           fis.close();
        }catch(IOException ioe)
        {
           ioe.printStackTrace();
           return;
        }catch(ClassNotFoundException c)
        {
           System.out.println("Class not found");
           c.printStackTrace();
           return;
        }
        System.out.println("Deserialized HashMap..");   
    	view.personTbl = view.tablePers(Bank.mapper);
        view.sp.setViewportView(view.personTbl);
        view.sp.revalidate();
        view.sp.repaint();
        view.personFrame.revalidate();
        view.personFrame.repaint();
        view.personFrame.add(view.sp,BorderLayout.CENTER); 
        
        view.accountTbl = view.tableAcc(Bank.mapper);
        view.spAcc.setViewportView(view.accountTbl);
        view.spAcc.revalidate();
        view.spAcc.repaint();
        view.accountFrame.revalidate();
        view.accountFrame.repaint();
        view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
        view.depFrame.setVisible(false);
    	
    }
}
