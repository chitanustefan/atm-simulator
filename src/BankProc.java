package tema4.tema4;

public interface BankProc {
	
    /**
     *  @pre p != null
     *  @post mapper.size() == mapper.size()@pre+1
     */
	public void addPerson(Person p);
	
	  /**
     *  @pre p != null
     *  @post mapper.size() == mapper.size()@pre-1
     */
	public void removePerson(Person p);
	
	  /**
     *  @pre p != null
     *  @pre a != null
     *  @post mapper.get(p).size() == mapper.size()@pre+1
     */
	public void addAcc(Account a, Person p);
	
	 /**
     *  @pre a != null
     *  @post mapper.get(p).size() == mapper.size()@pre-1
     */
	public void removeAcc(Person p, String iban);
	
	  /**
     *  @pre iban != null
     *  @pre sum >=0
     *  @pre c.equals("deposit") || c.equals("withdraw")
     *  @post sum1 != sum2
     */
	public void writeAcc(String iban, int sum, String c);
	

}

