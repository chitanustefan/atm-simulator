package tema4.tema4;

import java.util.ArrayList;

public class SavingAccount extends Account {

	public boolean with;
	public boolean dep;
	ArrayList<Person> listCustomers = new ArrayList<Person>();
	public SavingAccount(String iban, String holder, String hCNP, int suma, String tip) {
		super(iban, holder, hCNP, suma,tip);
		this.dep=false;
		this.with=false;
	}

	public void addRelatedCustomer(Person customer)
	{
		listCustomers.add(customer);
	}
	
	public void setInterst() {
		this.suma += this.suma*1/100;
	}
	
	public void withDraw(int sum) {
		if(this.with == false)
			if(this.suma >= sum) {
				this.suma=this.getSuma() - sum;
				notifyPersWith();
			}else System.out.println("Fonduri insuficiente");
		this.with = true;
		
	}
	
	public void deposit(int sum) {
		if(this.dep == false) {
			this.suma = this.getSuma() + sum;
			 notifyPersDep();
		}
		this.dep = true;
	}
	
	public void  notifyPersWith() {
	      for (Person observer : listCustomers) {
	          observer.notifyWith();
	       }
	      listCustomers.remove(0);
	}
	public void notifyPersDep() {
	      for (Person observer : listCustomers) {
	          observer.notifyDep();
	       }	
	      listCustomers.remove(0);
	}
}
