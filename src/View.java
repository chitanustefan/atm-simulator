package tema4.tema4;

import java.awt.*;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;



public class View {

	public JFrame editAccFrame, startFrame, personFrame, accountFrame, addPersFrame,viewPers,editPersFrame, addAccFrame, depFrame,withFrame;
	public JPanel insideStart, persBtnsPan, accountBtnPan,viewPersPan;
	public JButton intBtn, editAccOK, witOK,personBtn, accountBtn, addPerson, editPerson, editAccount, addAccount,addPersBTN, viewP, editPersOK,removePersOK,addAccOK, depositBtn,withBtn, depOK;
	public static JTable personTbl, accountTbl;
	public JTable accountsTbl;
	public JScrollPane sp, spAcc;
	public JLabel numePersAddL, prenumePersAddL, cnpPersAddL, numePersEditL, prenumePersEditL, cnpPersEditL;
	public JTextField numePersAddTxt, prenumePersAddTxt, cnpPersAddTxt,numePersEditTxt, prenumePersEditTxt, cnpPersEditTxt, ibanAddTxt,holderAddTxt, selDepIBAN ,selDepSumTxt;
	public JTextField withDrawIBANTXT, withDrawSumTxt, editAccIBANTxt,editAccTitTxt,editAccTipTxt, editAccSumTxt;
	public JRadioButton saving, spending;
	public ButtonGroup group;
	static JTable tab;	
	
	public View(Bank b) {
	
		GUI(b);	
		personBtn.setActionCommand("personClick");
		personBtn.addActionListener(new Controller(this));
		accountBtn.setActionCommand("accountClick");
		accountBtn.addActionListener(new Controller(this));
		addPerson.setActionCommand("addpers");
		addPerson.addActionListener(new Controller(this));
		addPersBTN.setActionCommand("okAddPers");
		addPersBTN.addActionListener(new Controller(this));
		editAccount.setActionCommand("editAcc");
		editAccount.addActionListener(new Controller(this));
		viewP.addActionListener(new Controller(this));
		viewP.setActionCommand("viewPers");
		editPerson.addActionListener(new Controller(this));
		editPerson.setActionCommand("editPers");
		editPersOK.addActionListener(new Controller(this));
		editPersOK.setActionCommand("editPersOK");
		removePersOK.addActionListener(new Controller(this));
		removePersOK.setActionCommand("removePersOK");
		addAccount.addActionListener(new Controller(this));
		addAccount.setActionCommand("addAccount");
		addAccOK.addActionListener(new Controller(this));
		addAccOK.setActionCommand("addAccOK");
		spending.addActionListener(new Controller(this));
		spending.setActionCommand("spending");
		saving.addActionListener(new Controller(this));
		saving.setActionCommand("saving");
		depositBtn.addActionListener(new Controller(this));
		depositBtn.setActionCommand("depBtn");
		withBtn.addActionListener(new Controller(this));
		withBtn.setActionCommand("withBtn");
		depOK.addActionListener(new Controller(this));
		depOK.setActionCommand("depOK");
		witOK.addActionListener(new Controller(this));
		witOK.setActionCommand("witOK");
		editAccOK.addActionListener(new Controller(this));
		editAccOK.setActionCommand("delAcc");
		intBtn.addActionListener(new Controller(this));
		intBtn.setActionCommand("interest");
	}
	
	public void GUI(Bank b) {
		
		//Begining Frame
		startFrame = new JFrame("Start");
		startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startFrame.setLayout(new BorderLayout());
		startFrame.setSize(400, 400);
		insideStart = new JPanel();
		insideStart.setLayout(new GridLayout(2,1));
		
		personBtn = new JButton("Persons");
		accountBtn = new JButton("Accounts");
		
		insideStart.add(personBtn);
		insideStart.add(accountBtn);
		startFrame.add(insideStart);
		
		
		//Account Frame
		accountFrame = new JFrame("Account");
		accountFrame.setLayout(new BorderLayout());
		accountFrame.setSize(900, 400);
		accountBtnPan = new JPanel();
		editAccount = new JButton("edit");
		addAccount = new JButton("add");
		depositBtn = new JButton("deposit");
		withBtn = new JButton("withdraw");
		accountTbl = new JTable();
		accountTbl = tableAcc(Bank.mapper);
		spAcc = new JScrollPane();
		spAcc.setViewportView(accountTbl);
		intBtn = new JButton("Interest");
		accountBtnPan.add(addAccount);
		accountBtnPan.add(editAccount);
		accountBtnPan.add(depositBtn);
		accountBtnPan.add(withBtn);
		accountBtnPan.add(intBtn);
		accountFrame.add(accountBtnPan, BorderLayout.NORTH);
		accountFrame.add(spAcc, BorderLayout.CENTER);
		
		//Edit acc Frame
		editAccFrame = new JFrame("Edit Account");
		editAccFrame.setLayout(new GridLayout(5,1));
		editAccFrame.setSize(400, 400);
		JPanel editAccPan = new JPanel();
		JLabel editAccIBANL = new JLabel("IBAN: ");
		editAccIBANTxt = new JTextField(26);
		editAccPan.add(editAccIBANL);
		editAccPan.add(editAccIBANTxt);
		JPanel editAccPan2 = new JPanel();
		JLabel editAccTitL = new JLabel("CNP: ");
		editAccTitTxt = new JTextField(13);
		editAccPan2.add(editAccTitL);
		editAccPan2.add(editAccTitTxt);
		JPanel editAccPan3 = new JPanel();
		JLabel editAccTipL = new JLabel("Tip: ");
		editAccTipTxt = new JTextField(10);
		editAccPan3.add(editAccTipL);
		editAccPan3.add(editAccTipTxt);
		JPanel editAccPan4 = new JPanel();
		JLabel editAccSumL = new JLabel("Suma: ");
		editAccSumTxt = new JTextField(10);
		editAccPan3.add(editAccSumL);
		editAccPan3.add(editAccSumTxt);
		
		editAccOK = new JButton("Sterge");
		
		editAccFrame.add(editAccPan);
		editAccFrame.add(editAccPan2);
		editAccFrame.add(editAccPan3);
		editAccFrame.add(editAccPan4);
		editAccFrame.add(editAccOK);
		
		//Deposit Frame
		depFrame = new JFrame("Deposit");
		depFrame.setLayout(new GridLayout(3,1));
		depFrame.setSize(380, 200);
		JPanel selIBAN = new JPanel();
		JPanel depSum = new JPanel();
		JLabel selIbanDep = new JLabel("IBAN: ");
		selDepIBAN = new JTextField(26);
		JLabel selSumL = new JLabel("Suma: ");
		selDepSumTxt = new JTextField(4);
		depOK = new JButton("Deposit");
		depSum.add(selSumL);
		depSum.add(selDepSumTxt);
		selIBAN.add(selIbanDep);
		selIBAN.add(selDepIBAN);
		
		depFrame.add(selIBAN);
		depFrame.add(depSum);
		depFrame.add(depOK);
		
		//WithDraw Frame
		withFrame = new JFrame("Withdraw");
		withFrame.setLayout(new GridLayout(3,1));
		withFrame.setSize(380, 200);
		JPanel selIBANWit = new JPanel();
		JPanel witSum = new JPanel();
		JLabel selIbanWitL = new JLabel("IBAN: ");
		JLabel witSumL = new JLabel("Suma: ");
		withDrawIBANTXT = new JTextField(26);
		withDrawSumTxt = new JTextField(10);
		witOK = new JButton("Withdraw");
		
		selIBANWit.add(selIbanWitL);
		selIBANWit.add(withDrawIBANTXT);
		witSum.add(witSumL);
		witSum.add(withDrawSumTxt);
		withFrame.add(selIBANWit);
		withFrame.add(witSum);
		withFrame.add(witOK);
		
		
		//Add Account
		addAccFrame = new JFrame("Add Account");
		addAccFrame.setLayout(new GridLayout(3,1));
		addAccFrame.setSize(400, 400);
		JPanel holderPan = new JPanel();
		JLabel holderL = new JLabel("CNP titular: ");
		holderAddTxt = new JTextField(30);
		holderPan.add(holderL);
		holderPan.add(holderAddTxt);
		JPanel radioBut = new JPanel();
		spending = new JRadioButton();
		spending.setText("Spending");
		saving = new JRadioButton();
		saving.setText("Saving");
		group = new ButtonGroup();
		group.add(saving);
		group.add(spending);
		radioBut.add(saving);
		radioBut.add(spending);
		addAccOK = new JButton("Adauga Cont");
		addAccFrame.add(holderPan);
		addAccFrame.add(radioBut);
		addAccFrame.add(addAccOK);
		
		//Person Frame
		personFrame = new JFrame("Person");
		personFrame.setLayout(new BorderLayout());
		personFrame.setSize(400, 400);
		persBtnsPan = new JPanel();
		viewPers = new JFrame("View Pers");
		viewPers.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		viewPers.setLayout(new BorderLayout());
		viewPers.setSize(400, 400);
		viewPersPan = new JPanel();
		addPerson = new JButton("add");
		editPerson = new JButton("edit");
		viewP = new JButton("view");
		
		personTbl = new JTable();
		personTbl = tablePers(Bank.mapper);
		sp = new JScrollPane();
		sp.setViewportView(personTbl);
		
		persBtnsPan.add(addPerson);
		persBtnsPan.add(editPerson);
		persBtnsPan.add(viewP);
		personFrame.add(persBtnsPan, BorderLayout.NORTH);
		personFrame.add(sp,BorderLayout.CENTER);
		
		
		
		//Edit Pers Frame
		editPersFrame = new JFrame("Edit Person");
		editPersFrame.setLayout((new GridLayout(4,1)));
		editPersFrame.setSize(400, 300);
		JPanel numePersEdit = new JPanel();
		JPanel prenumePersEdit = new JPanel();
		JPanel cnpPersEdit = new JPanel();
		JPanel btnsEDIT = new JPanel();
		numePersEditL = new JLabel("Nume: ");
		numePersEditTxt = new JTextField(20);	
		prenumePersEditL = new JLabel("Prenume: ");
		prenumePersEditTxt = new JTextField(20);
		cnpPersEditL = new JLabel("CNP: ");
		cnpPersEditTxt = new JTextField(20);
		editPersOK = new JButton("Editeaza");
		removePersOK = new JButton("Sterge");
		numePersEdit.add(numePersEditL);
		numePersEdit.add(numePersEditTxt);
		prenumePersEdit.add(prenumePersEditL);
		prenumePersEdit.add(prenumePersEditTxt);
		cnpPersEdit.add(cnpPersEditL);
		cnpPersEdit.add(cnpPersEditTxt);
		btnsEDIT.add(editPersOK);
		btnsEDIT.add(removePersOK);
		editPersFrame.add(numePersEdit);
		editPersFrame.add(prenumePersEdit);
		editPersFrame.add(cnpPersEdit);
		editPersFrame.add(btnsEDIT);
		
		//Add Pers Frame
		addPersFrame = new JFrame("Add Person");
		addPersFrame.setLayout(new GridLayout(4,1));
		addPersFrame.setSize(400, 300);
		JPanel numePersAdd = new JPanel();
		JPanel prenumePersAdd = new JPanel();
		JPanel cnpPersAdd = new JPanel();
		//JPanel tiContPersAdd = new JPanel();
		
		numePersAddL = new JLabel("Nume: ");
		prenumePersAddL = new JLabel("Prenume: ");
		cnpPersAddL = new JLabel("CNP: ");
		numePersAddTxt = new JTextField(20);
		prenumePersAddTxt = new JTextField(20);
		cnpPersAddTxt = new JTextField(20);
		addPersBTN = new JButton("Adauga");
		numePersAdd.add(numePersAddL);
		numePersAdd.add(numePersAddTxt);
		prenumePersAdd.add(prenumePersAddL);
		prenumePersAdd.add(prenumePersAddTxt);
		cnpPersAdd.add(cnpPersAddL);
		cnpPersAdd.add(cnpPersAddTxt);
		addPersFrame.add(numePersAdd);
		addPersFrame.add(prenumePersAdd);
		addPersFrame.add(cnpPersAdd);
		addPersFrame.add(addPersBTN);
		
		
		startFrame.setVisible(true);
		personFrame.setVisible(false);
		accountFrame.setVisible(false);
		addPersFrame.setVisible(false);
		viewPers.setVisible(false);
		editPersFrame.setVisible(false);
		addAccFrame.setVisible(false);
		editAccFrame.setVisible(false);
	}
	
	
	public static <T> JTable tablePers(HashMap<Person, ArrayList<Account>> map) {
	    DefaultTableModel model = new DefaultTableModel(
	        new Object[] { "Nume", "Prenume", "CNP" }, 0);
	    for (HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet()) {
	        model.addRow(new String[] { entry.getKey().getNume(), entry.getKey().getPrenume(), entry.getKey().getCnp() });
	    }
	    tab = new JTable(model);
		return tab;
	}
	
	public static <T> JTable tableAcc(HashMap<Person, ArrayList<Account>> map) {
	    DefaultTableModel model = new DefaultTableModel(
	        new Object[] { "IBAN", "Titular", "CNP", "Suma", "Tip" }, 0);
	    for (HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet()) {
	    	Iterator<Account> i = entry.getValue().iterator();
	    	while(i.hasNext())
	    	{
	    		Account a = i.next();
	    		model.addRow(new Object[] {a.getIban(),a.getHolder(), a.gethCNP(), a.getSuma(),a.getTip() });
	    	}   
	    }
	    tab = new JTable(model);
		return tab;
	}
	
	public void printAccount(HashMap<Person, ArrayList<Account>> map) {
	    for (HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet()) {
	    	Iterator<Account> i = entry.getValue().iterator();
	    	while(i.hasNext())
	    	{
	    		Account a = i.next();
	    		System.out.println("IBAN: "+a.getIban()+" "+"Titular "+a.getHolder()+" "+"Suma: "+a.getSuma());
	    	}
	    //	System.out.println(entry.getValue());
	    }
	}
	
	public void printHash(HashMap<Person, ArrayList<Account>> map) {
	    for (HashMap.Entry<Person, ArrayList<Account>> entry : map.entrySet()) {
	       System.out.println(entry.getKey().getNume() + " "+ entry.getKey().getPrenume()+" "+ entry.getKey().getCnp());
	    }
	}
	
}
