package tema4.tema4;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Controller implements ActionListener, Serializable {

	View view;
	Bank b = new Bank();
	Person oldP;
	public Controller(View view) {
		this.view = view;
	}

	    
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
        if(command.equals("personClick")) {
       	 view.personFrame.setVisible(true);
        }
        if(command.equals("accountClick")) {
        	view.accountFrame.setVisible(true);
        }
        if(command.equals("addpers")) {
        	view.addPersFrame.setVisible(true);
        }
        if(command.equals("addAccount")) {
        	view.addAccFrame.setVisible(true);
        }
        if(command.equals("addAccOK")) {
        	boolean cnt = false;
        	Person x = new Person(null,null,null);
        	for (HashMap.Entry<Person, ArrayList<Account>> entry : Bank.mapper.entrySet()) {
        		String cnp = entry.getKey().getCnp();
        		if(cnp.equals(view.holderAddTxt.getText())){
        				x = new Person(entry.getKey().getNume(), entry.getKey().getPrenume(), entry.getKey().getCnp());
        				cnt = true;
        		}
        	}
        	if(cnt) {
        		if(view.group.getSelection().getActionCommand()=="saving") {
            		Account a = new SavingAccount(ibanGenerator(), x.getNume(), x.getCnp(), 0, "Saving");
            		b.addAcc(a,x);
            		view.printAccount(Bank.mapper);
            	}
            	if(view.group.getSelection().getActionCommand()=="spending") {
            		Account a = new SpendingAccount(ibanGenerator(), x.getNume(), x.getCnp(), 0, "Spending");
            		b.addAcc(a,x);
            		view.printAccount(Bank.mapper);
            	}
        	}else {
        		System.out.println("Persoana nu a fost gasita");
        	}
        	ser();
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
        	
        	view.addAccFrame.setVisible(false);
        }
        if(command.equals("okAddPers")) {
        	Person p = new Person(view.numePersAddTxt.getText(),view.prenumePersAddTxt.getText(),view.cnpPersAddTxt.getText());
        	b.addPerson(p);
        	view.printHash(Bank.mapper);
           	view.personTbl = view.tablePers(Bank.mapper);
            view.sp.setViewportView(view.personTbl);
            view.sp.revalidate();
            view.sp.repaint();
            view.personFrame.revalidate();
            view.personFrame.repaint();
            view.personFrame.add(view.sp,BorderLayout.CENTER);
            ser();
        	view.addPersFrame.setVisible(false);
        }
        if(command.equals("removePersOK")) {
        	Person p = new Person(view.numePersEditTxt.getText(), view.prenumePersEditTxt.getText(), view.cnpPersEditTxt.getText());
        	b.removePerson(p);
        	view.printHash(Bank.mapper);
        	View.personTbl = View.tablePers(Bank.mapper);
            //view.sp = new JScrollPane(view.personTbl);
            view.sp.setViewportView(View.personTbl);
            view.sp.revalidate();
            view.sp.repaint();
            view.personFrame.revalidate();
            view.personFrame.repaint();
            view.personFrame.add(view.sp,BorderLayout.CENTER);
            ser();
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
            view.depFrame.setVisible(false);
        	view.editPersFrame.setVisible(false);
        }
        if(command.equals("editAcc")) {
        	view.editAccFrame.setVisible(true);
        }
        if(command.equals("delAcc")) {
        	
        	Person x = new Person(null,null,null);
        	for (HashMap.Entry<Person, ArrayList<Account>> entry : Bank.mapper.entrySet()) {
        		String cnp = entry.getKey().getCnp();
        		if(cnp.equals(view.editAccTitTxt.getText())){
        				x = new Person(entry.getKey().getNume(), entry.getKey().getPrenume(), entry.getKey().getCnp());
        		}
        	}
    		b.removeAcc(x, view.editAccIBANTxt.getText());
    		ser();
    		view.printAccount(Bank.mapper);
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
            view.depFrame.setVisible(false);
        	view.editAccFrame.setVisible(false);
        }
        if(command.equals("editPers")) {
        	view.editPersFrame.setVisible(true);
        }
        if(command.equals("editPersOK")) {
        	
        	Person p = new Person(view.numePersEditTxt.getText(), view.prenumePersEditTxt.getText(), view.cnpPersEditTxt.getText());
        	b.editPerson(oldP, p);
        	view.printHash(Bank.mapper);
        	View.personTbl = View.tablePers(Bank.mapper);
            //view.sp = new JScrollPane(view.personTbl);
            view.sp.setViewportView(View.personTbl);
            view.sp.revalidate();
            view.sp.repaint();
            view.personFrame.revalidate();
            view.personFrame.repaint();
            view.personFrame.add(view.sp,BorderLayout.CENTER);
            ser();
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
            view.depFrame.setVisible(false);
        	view.editPersFrame.setVisible(false);
        	
        }
        if(command.equals("viewPers")) {
        	view.printHash(Bank.mapper);
            view.viewPers.setVisible(true);
        }
        if(command.equals("depBtn")) {
            view.depFrame.setVisible(true);
        }
        if(command.equals("interest")) {
        	for (HashMap.Entry<Person, ArrayList<Account>> entry : Bank.mapper.entrySet()) {
        		Iterator<Account> it = entry.getValue().iterator();
        		while(it.hasNext()) {
        			Account a = it.next();
        			if(a.getTip().equals("Saving"))
        				if(a instanceof SavingAccount) {
        					SavingAccount a2 = (SavingAccount) a;
        					a2.setInterst();
        					ser();
        					view.accountTbl = view.tableAcc(Bank.mapper);
        		            view.spAcc.setViewportView(view.accountTbl);
        		            view.spAcc.revalidate();
        		            view.spAcc.repaint();
        		            view.accountFrame.revalidate();
        		            view.accountFrame.repaint();
        		            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
        				}
        		}
        	}
        }
        if(command.equals("witOK")) {
        	
        	b.writeAcc(view.withDrawIBANTXT.getText(), Integer.parseInt(view.withDrawSumTxt.getText()), "withdraw");
        	ser();
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
            view.withFrame.setVisible(false);
        }
        if(command.equals("depOK")) {	
        	b.writeAcc(view.selDepIBAN.getText(),Integer.parseInt(view.selDepSumTxt.getText()), "deposit");
        	ser();
            view.accountTbl = view.tableAcc(Bank.mapper);
            view.spAcc.setViewportView(view.accountTbl);
            view.spAcc.revalidate();
            view.spAcc.repaint();
            view.accountFrame.revalidate();
            view.accountFrame.repaint();
            view.accountFrame.add(view.spAcc,BorderLayout.CENTER);
            view.depFrame.setVisible(false);
        }
        if(command.equals("withBtn")) {
            view.withFrame.setVisible(true);
        }
        View.personTbl.addMouseListener(new java.awt.event.MouseAdapter() { // row is clicked
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            	
            	DefaultTableModel model = (DefaultTableModel) View.personTbl.getModel();
                int selectedRowIndex = View.personTbl.getSelectedRow();
               // System.out.println("3333");
                String nume =  (String) model.getValueAt(selectedRowIndex, 0);
                String prenume = (String) model.getValueAt(selectedRowIndex, 1);
                String cnp = (String) model.getValueAt(selectedRowIndex, 2);                     
                view.numePersEditTxt.setText(nume);
                view.prenumePersEditTxt.setText(prenume);
                view.cnpPersEditTxt.setText(cnp);
                Person oldP = new Person(nume, prenume, cnp);
            }
        });
        View.accountTbl.addMouseListener(new java.awt.event.MouseAdapter() { // row is clicked
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            	
            	DefaultTableModel model = (DefaultTableModel) View.accountTbl.getModel();
                int selectedRowIndex = View.accountTbl.getSelectedRow();
               // System.out.println("3333");
                String iban =  (String) model.getValueAt(selectedRowIndex, 0); 
                String cnp =  (String) model.getValueAt(selectedRowIndex, 2); 
                int suma =  (Integer) model.getValueAt(selectedRowIndex, 3); 
                String sum = Integer.toString(suma);
                String tip = (String) model.getValueAt(selectedRowIndex, 4);
                view.selDepIBAN.setText(iban);
                view.withDrawIBANTXT.setText(iban);
                view.editAccIBANTxt.setText(iban);
                view.editAccTitTxt.setText(cnp);
                view.editAccSumTxt.setText(sum);
                view.editAccTipTxt.setText(tip);
                
            }
        });
	}
	
	public void ser() {
		 try
         {
                FileOutputStream fos = new FileOutputStream("hashmap.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(Bank.mapper);
                oos.close();
                fos.close();
         }catch(IOException ioe)
          {
                ioe.printStackTrace();
          }
	}
	
	public String ibanGenerator() {
		Random r = new Random();
		String x = Integer.toString(r.nextInt(10));
		//RO12BTRL 0130120262473400  16
		String iban = "RO"+x+"BTRLCRT";	
		for(int i =1;i<=16;i++) {
			iban += Integer.toString(r.nextInt(10));
		}
		return iban;
	}
	
}
