package tema4.tema4;

import java.util.ArrayList;

public class SpendingAccount extends Account{

	ArrayList<Person> listCustomers = new ArrayList<Person>();
	public SpendingAccount(String iban, String holder, String hCNP,int suma,String tip) {
		super(iban, holder, hCNP, suma,tip);
		// TODO Auto-generated constructor stub
	}

	public void addRelatedCustomer(Person customer)
	{
		listCustomers.add(customer);
	}
	
	public void withDraw(int sum) {
		if(this.getSuma() - sum >=0)
			this.suma = this.getSuma() - sum;
		else System.out.println("Fonduri insuficiente");
		notifyPersWith();
	}
	
	public void deposit(int sum) {
		this.suma = this.getSuma() + sum;
		notifyPersDep();
	}

	public void  notifyPersWith() {
	      for (Person observer : listCustomers) {
	          observer.notifyWith();
	       }	
	      listCustomers.remove(0);
	}
	public void notifyPersDep() {
		
	      for (Person observer : listCustomers) {
	          observer.notifyDep();
	       }	
	      listCustomers.removeAll(listCustomers);
	}
}
