package tema4.tema4;

public abstract class Observer {
	
	public abstract void notifyWith();
	public abstract void notifyDep();
}
