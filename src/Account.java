package tema4.tema4;

import java.util.ArrayList;
import java.util.Random;

public class Account implements java.io.Serializable{

	public String iban;
	public String holder;
	public String hCNP;
	public int suma;
	public String tip;
	
	
	public Account(String iban, String holder, String hCNP, int suma, String tip) {
		
		this.iban = iban;
		this.holder = holder;
		this.suma = suma;
		this.tip = tip;
		this.hCNP = hCNP;
	}
	
	public String getIban() {
		return iban;
	}


	public void setIban(String iban) {
		this.iban = iban;
	}


	public String getHolder() {
		return holder;
	}


	public void setHolder(String holder) {
		this.holder = holder;
	}


	public int getSuma() {
		return suma;
	}


	public void setSuma(int suma) {
		this.suma = suma;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String gethCNP() {
		return hCNP;
	}

	public void sethCNP(String hCNP) {
		this.hCNP = hCNP;
	}

	

}
